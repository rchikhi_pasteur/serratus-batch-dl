aws cloudformation create-stack --stack-name serratus-batch-dl --template-body file://template/template.yaml --capabilities CAPABILITY_NAMED_IAM
# https://gist.github.com/ambakshi/d17ad18e0532530359f9
aws configure set preview.sdb true
aws sdb create-domain --domain-name serratus-batch

