# Download SRA reads and putting them on another S3 bucket, converted to fastq, on AWS Batch

### Source

Similar to https://gitlab.pasteur.fr/rchikhi_pasteur/serratus-batch-assembly and references therein.

Aims at similar functionality as https://gitlab.pasteur.fr/rchikhi_pasteur/serratus-batch-assembly

### Installation 

Execute the below commands to spin up the infrastructure cloudformation stack.

```
./spinup.sh
./deploy-docker.sh
```

If you ever recreate the stack (e.g. after `cleanup.sh`), you don't need to run `deploy-docker.sh` unless the Dockerfile or scripts in `src/` were modified.

### Running a download job

1. `./submit_job.py SRRxxxxxx`
2. In AWS Console > Batch, monitor how the job runs.

### Code Cleanup

In short:

```
./cleanup.sh
```

Which deletes the CloudFormation stack.

What it doesn't do (need sto be done manually):

AWS Console > ECR - serratus-dl-batch-job - delete the image(s) that are pushed to the repository

## License

This library is licensed under the MIT-0 License. See the LICENSE file.

